{
  description = "My Rust Development Environment";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs =
    {
      self,
      nixpkgs,
      flake-utils,
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = import nixpkgs {
          inherit system;
          config.allowUnfreePredicate =
            pkg: pkg.pname == "vscode" || pkg.pname == "vscode-extension-fill-labs-dependi";
        };
      in
      {
        devShells.default = pkgs.mkShell {
          buildInputs =
            (with pkgs; [
              alsa-lib
              ldtk
              rustup
              pkg-config
              udev
              libudev-zero
              xorg.libX11
              xorg.libXcursor
              xorg.libXrandr
              xorg.libXi
              libxkbcommon
              vulkan-loader
              libGL
              vscode
            ])
            ++ (with pkgs.vscode-extensions; [
              rust-lang.rust-analyzer
              fill-labs.dependi
            ]);

          shellHook = ''
            export LD_LIBRARY_PATH="${
              pkgs.lib.makeLibraryPath (
                with pkgs;
                [
                  alsa-lib
                  udev
                  vulkan-loader
                  libGL
                  libxkbcommon
                ]
              )
            }:$LD_LIBRARY_PATH"
            export PKG_CONFIG_PATH="${pkgs.alsa-lib}/lib/pkgconfig:${pkgs.libudev-zero}/lib/pkgconfig:$PKG_CONFIG_PATH"
            export RUSTUP_HOME=~/.rustup
            export PATH=~/.rustup/bin:$PATH
            rustup toolchain install stable
            rustup default stable
          '';
        };
      }
    );
}
