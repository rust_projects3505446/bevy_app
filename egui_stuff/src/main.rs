use bevy::{
    prelude::{App, Update},
    DefaultPlugins,
};
use bevy_egui::{
    egui::{Button, CentralPanel, Color32, RichText, TopBottomPanel, Window},
    EguiContexts, EguiPlugin,
};
fn main() {
    App::new()
        .add_plugins((DefaultPlugins, EguiPlugin))
        .add_systems(Update, (ui_example_system, header_bar, central_panel))
        .run();
}
fn ui_example_system(mut contexts: EguiContexts) {
    Window::new("Hello").show(contexts.ctx_mut(), |ui| {
        ui.label("world");
    });
}

fn header_bar(mut contexts: EguiContexts) {
    TopBottomPanel::top("header_bar").show(contexts.ctx_mut(), |ui| {
        // Create an array of button labels for easier management
        ui.horizontal(|ui| {
            [
                ("Button 1", Color32::GREEN, Color32::RED),
                ("Button 2", Color32::BLUE, Color32::GOLD),
                ("Button 3", Color32::RED, Color32::BLUE),
                (
                    "Button 4",
                    Color32::YELLOW,
                    Color32::from_rgb(238, 130, 238),
                ),
                ("Button 5", Color32::DARK_RED, Color32::DARK_BLUE),
            ]
            .into_iter()
            .map(|(label, button_color, label_color)| {
                (
                    ui.add(Button::new(RichText::new(label).color(label_color)).fill(button_color)),
                    label,
                )
            })
            .for_each(|(button, label)| {
                if button.clicked() {
                    // Handle button clicks here
                    println!("Clicked button: {}", label);
                }
            });
        });
    });
}
fn central_panel(mut contexts: EguiContexts) {
    CentralPanel::default().show(contexts.ctx_mut(), |ui| {
        ui.label("Hello World!");
    });
}
