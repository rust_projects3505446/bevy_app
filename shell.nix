let
  pkgs = import (fetchTarball "channel:nixpkgs-unstable") { };
  overrides = (builtins.fromTOML (builtins.readFile ./rust-toolchain.toml));
  libPath =
    with pkgs;
    lib.makeLibraryPath [
      # load external libraries that you need in your rust project here
    ];
in
pkgs.mkShell {
  buildInputs = with pkgs; [
    rustup
    pkg-config
    alsa-lib
    udev
    libudev-zero
    xorg.libX11
    xorg.libXcursor
    xorg.libXrandr
    xorg.libXi
    libxkbcommon
    vulkan-loader
    libGL
  ];

  shellHook = ''
    export LD_LIBRARY_PATH="${
      pkgs.lib.makeLibraryPath [
        pkgs.alsa-lib
        pkgs.udev
        pkgs.vulkan-loader
        pkgs.libGL
        pkgs.libxkbcommon
      ]
    }:$LD_LIBRARY_PATH"
    export PKG_CONFIG_PATH="${pkgs.alsa-lib}/lib/pkgconfig:${pkgs.libudev-zero}/lib/pkgconfig:$PKG_CONFIG_PATH"
    export RUSTUP_HOME=~/.rustup
    export PATH=~/.rustup/bin:$PATH
    rustup toolchain install ${overrides.toolchain.channel}
    rustup default ${overrides.toolchain.channel}
  '';
}
