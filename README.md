<img src="./images/ferris.png" alt="../images/ferris.png" height="100"/> <img src="./images/bevy.png" alt="../images/bevy.png" height="100"/>
# Bevy Examples in Rust
The main goal of this repository is to familiarise yourself with the [RUST](https://www.google.com/url?sa=t&source=web&rct=j&opi=89978449&url=https://www.rust-lang.org/&ved=2ahUKEwivkNP64M-HAxVahP0HHXgZAL4QFnoECBgQAQ&usg=AOvVaw0Eo-jPMg1YR89Z4OceiP9Q) based [bevy](https://www.google.com/url?sa=t&source=web&rct=j&opi=89978449&url=https://bevyengine.org/&ved=2ahUKEwjCxLPP4M-HAxV9hP0HHf9SECgQFnoECAYQAQ&usg=AOvVaw3ei00vzoL4ohNCxPKmDQTT) [game engine](https://www.google.com/url?sa=t&source=web&rct=j&opi=89978449&url=https://en.wikipedia.org/wiki/Game_engine&ved=2ahUKEwj0mtLk4M-HAxXH87sIHdm2AqwQFnoECBsQAQ&usg=AOvVaw2SGYqEQ4hc5g74Xcj2jZVt) and to create a few application examples.

## Getting started
Rust must be installed first. This usually works like this under [Linux](https://www.google.com/url?sa=t&source=web&rct=j&opi=89978449&url=https://www.linux.org/&ved=2ahUKEwiLxtjL4c-HAxV8hf0HHTcRMQQQFnoECBYQAQ&usg=AOvVaw0OoiTmQ34Do398976duTL0):
```sh
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

