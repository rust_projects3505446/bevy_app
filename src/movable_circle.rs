#![allow(unused)]
use bevy::{
    app::{App, Startup, Update},
    core_pipeline::core_2d::Camera2d,
    ecs::{change_detection::Res, prelude::With},
    input::{keyboard::KeyCode, ButtonInput},
    math::Vec3,
    prelude::{
        Assets, Circle, Color, ColorMaterial, Commands, Component, Mesh, Mesh2d, MeshMaterial2d,
        Query, ResMut, Srgba, Transform,
    },
    DefaultPlugins,
};

pub const MOVE_SPEED: f32 = 6.0;
#[derive(Component)]
pub struct Player;
fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_systems(Startup, (setup_player, setup_camera))
        .add_systems(Update, move_player)
        .run();
}
pub fn setup_camera(mut commands: Commands) {
    commands.spawn(Camera2d);
}

fn setup_player(
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut commands: Commands,
) {
    commands.spawn((
        Player,
        Mesh2d(meshes.add(Circle { radius: 10.0 })),
        MeshMaterial2d(materials.add(Color::Srgba(Srgba::RED))),
    ));
}
pub fn move_player(
    mut transforms: Query<&mut Transform, With<Player>>,
    keys: Res<ButtonInput<KeyCode>>,
) {
    transforms.iter_mut().for_each(|mut transform| {
        let direction: Vec3 = Vec3::new(
            (keys.pressed(KeyCode::KeyD)as i32 - keys.pressed(KeyCode::KeyA) as i32)as f32,
            (keys.pressed(KeyCode::KeyW) as i32 - keys.pressed(KeyCode::KeyS) as i32)as f32,
            0.0
        );

        if 0.0 < direction.length() {
            transform.translation += MOVE_SPEED * direction.normalize();
        }
    });
}
