use bevy::{
    app::{App, Startup, Update},
    color::{Color, Srgba},
    ecs::change_detection::Res,
    input::{keyboard::KeyCode, ButtonInput},
    math::Vec3,
    prelude::{
        default, Assets, BuildChildren, Camera2dBundle, Circle, ColorMaterial, Commands, Component,
        FlexDirection, Mesh, MeshMaterial2d, Name, Node, Query, ResMut, Style, Text, TextBundle,
        TextSection, TextStyle, Transform, Val, Window, With,
    },
    sprite::{MaterialMesh2dBundle, Mesh2dHandle},
    DefaultPlugins,
};
#[derive(Component)]
pub struct Entity;
#[derive(Component)]
pub struct Player;
#[derive(Component)]
pub struct PositionScore;
#[derive(Component)]
pub struct DirectionScore;

use rand::{rngs::ThreadRng, Rng};
fn bundle_press(width: f32, height: f32) -> Node {
    Node {
        style: Style {
            width: Val::Percent(width),
            height: Val::Percent(height),
            flex_direction: FlexDirection::Column,
            ..default()
        },
        background_color: Color::srgba(0.0, 0.0, 0.0, 0.0).into(),
        ..Default::default()
    }
}
fn setup_camera(mut commands: Commands) {
    commands.spawn(Camera2dBundle::default());
}
fn setup_instructions(mut commands: Commands, windows: Query<&mut Window>) {
    let root = commands.spawn(bundle_press(100.0, 20.0)).id();
    let child1 = commands.spawn(bundle_press(50.0, 50.0)).id();
    let child2 = commands.spawn(bundle_press(100.0, 200.0)).id();
    let default_style: TextStyle = TextStyle {
        font_size: 30000.0 / windows.single().resolution.width(),
        color: Color::srgb(0.5, 0.5, 1.0),
        ..default()
    };
    let default_section: TextSection = TextSection::from_style(TextStyle {
        font_size: 30000.0 / windows.single().resolution.width(),
        color: Color::srgb(1.0, 0.5, 0.5),
        ..default()
    });
    let position_text = commands
        .spawn((
            PositionScore,
            TextBundle::from_sections([
                TextSection::new("Position:", default_style.clone()),
                default_section.clone(),
            ]),
        ))
        .insert(Name::new("Position"))
        .id();
    let direction_text = commands
        .spawn((
            DirectionScore,
            TextBundle::from_sections([
                TextSection::new("Direction:", default_style.clone()),
                default_section,
            ]),
        ))
        .insert(Name::new("Direction"))
        .id();
    commands.entity(child1).add_child(position_text);
    commands.entity(child2).add_child(direction_text);
    commands.entity(root).add_child(child1).add_child(child2);
}
fn setup_entity(
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut commands: Commands,
) {
    commands.spawn((
        Player,
        MaterialMesh2dBundle {
            mesh: Mesh2dHandle(meshes.add(Circle { radius: 10.0 })),
            material: MeshMaterial2d(materials.add(Color::Srgba(Srgba::GREEN))),
            ..default()
        },
    ));
    let rng = rand::thread_rng();
    (0..100 * rng.clone().gen_range(1..10)).for_each(|_| {
        commands.spawn((
            Entity,
            MaterialMesh2dBundle {
                mesh: Mesh2dHandle(meshes.add(Circle { radius: 10.0 })),
                material: MeshMaterial2d(materials.add(Color::Srgba(Srgba::rgb(
                    rng.clone().gen(),
                    rng.clone().gen(),
                    rng.clone().gen(),
                )))),
                ..default()
            },
        ));
    });
}
fn print_player(
    transforms: Query<&mut Transform, With<Player>>,
    mut position: Query<&mut Text, With<PositionScore>>,
) {
    transforms.iter().for_each(|transform| {
        position.single_mut().sections[1].value = format!(
            "x:{} y:{}",
            transform.translation.x, transform.translation.y
        );
    });
}
const MOVE_SPEED: f32 = 6.0;
fn move_player(
    mut transforms: Query<&mut Transform, With<Player>>,
    keys: Res<ButtonInput<KeyCode>>,
    mut dir_text: Query<&mut Text, With<DirectionScore>>,
) {
    let mut direction: Vec3 = Vec3::ZERO;

    transforms.iter_mut().for_each(|mut transform| {
        if keys.pressed(KeyCode::KeyW) {
            direction.y += 1.0;
        }
        if keys.pressed(KeyCode::KeyS) {
            direction.y -= 1.0;
        }
        if keys.pressed(KeyCode::KeyA) {
            direction.x -= 1.0;
        }
        if keys.pressed(KeyCode::KeyD) {
            direction.x += 1.0;
        }

        dir_text.single_mut().sections[1].value = format!("x:{} y:{}", direction.x, direction.y);
        if 0.0 < direction.length() {
            transform.translation += MOVE_SPEED * direction.normalize();
        }
    });
}
fn calculate_translation(border: f32, current_number: f32, mut rng: ThreadRng) -> f32 {
    if current_number >= border / 2.0 {
        current_number - rng.gen_range(0.0..40.0)
    } else if current_number <= -border / 2.0 {
        current_number + rng.gen_range(0.0..40.0)
    } else {
        current_number + rng.gen_range(-40.0..40.0)
    }
}
fn move_entity(mut transforms: Query<&mut Transform, With<Entity>>, windows: Query<&mut Window>) {
    let rng = rand::thread_rng();
    transforms.iter_mut().for_each(|mut transform| {
        transform.translation.x = calculate_translation(
            windows.single().resolution.width(),
            transform.translation.x,
            rng.clone(),
        );
        transform.translation.y = calculate_translation(
            windows.single().resolution.height(),
            transform.translation.y,
            rng.clone(),
        );
    });
}
fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_systems(Startup, (setup_entity, setup_camera, setup_instructions))
        .add_systems(Update, (move_player, move_entity, print_player))
        .run();
}
