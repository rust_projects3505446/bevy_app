use bevy::{
    app::Startup,
    asset::Assets,
    core_pipeline::core_2d::Camera2d,
    ecs::{
        change_detection::{Res, ResMut},
        component::Component,
        prelude::{Commands, Query},
    },
    gizmos::gizmos::Gizmos,
    input::{mouse::MouseButton, ButtonInput},
    prelude::{
        default, App, Camera, Circle, Color, ColorMaterial, DefaultPlugins, GlobalTransform,
        Handle, Mesh, Mesh2d, MeshMaterial2d, Srgba, Text, TextFont, Transform, Update, Vec2,
        Window,
    },
};
#[derive(Component)]
struct RedCircle;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_systems(Startup, setup_camera)
        .add_systems(Update, (draw_cursor, mouse_button_input))
        .run();
}
fn setup_camera(mut commands: Commands) {
    commands.spawn(Camera2d);
}

fn draw_cursor(
    camera_query: Query<(&Camera, &GlobalTransform)>,
    windows: Query<&Window>,
    mut gizmos: Gizmos,
) {
    let (camera, camera_transform) = camera_query.single();

    let Some(cursor_position) = windows.single().cursor_position() else {
        return;
    };

    // Calculate a world position based on the cursor's position.
    match camera.viewport_to_world_2d(camera_transform, cursor_position) {
        Ok(point) => gizmos.circle_2d(point, 5., Srgba::WHITE),
        Err(_err) => panic!("can't find viewport"),
    };
}
fn mouse_button_input(
    buttons: Res<ButtonInput<MouseButton>>,
    camera_query: Query<(&Camera, &GlobalTransform)>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut meshes: ResMut<Assets<Mesh>>,
    windows: Query<&Window>,
    mut commands: Commands,
) {
    if buttons.pressed(MouseButton::Left) {
        let (camera, camera_transform) = camera_query.single();
        let cursor_position: Vec2 = match windows.single().cursor_position() {
            Some(val) => val,
            None => panic!("no cursor Position"),
        };

        // Calculate a world position based on the cursor's position.
        let point: Vec2 = match camera.viewport_to_world_2d(camera_transform, cursor_position) {
            Ok(val) => val,
            Err(_err) => panic!("no valid data"),
        };

        // Spawn a red circle at the cursor position
        commands.spawn((
            Mesh2d(meshes.add(Circle { radius: 2.0 })),
            MeshMaterial2d(materials.add(Color::Srgba(Srgba::RED))),
            Transform::from_translation(point.extend(0.0)),
            RedCircle,
        ));
    }
    if buttons.just_released(MouseButton::Left) {
        let (camera, camera_transform) = camera_query.single();
        let cursor_position: Vec2 = match windows.single().cursor_position() {
            Some(val) => val,
            None => return,
        };

        // Calculate a world position based on the cursor's position.
        let point: Vec2 = match camera.viewport_to_world_2d(camera_transform, cursor_position) {
            Ok(val) => val,
            Err(_err) => panic!("no valid data"),
        };
        // Remove all red circles
        commands.spawn((
            Text(format!(
                "x:{},
                 y:{}",
                point.x, point.y
            )),
            TextFont {
                font: Handle::default(),
                font_size: 32.0,
                ..default()
            },
            Transform::from_translation(point.extend(0.0)),
        ));
    }
}
