use bevy::{
    app::{App, Startup, Update},
    color::{Color, Srgba},
    ecs::change_detection::Res,
    input::{keyboard::KeyCode, ButtonInput},
    math::Vec3,
    prelude::{
        default, Assets, Camera2d, Circle, ColorMaterial, Commands, Mesh, Mesh2d, MeshMaterial2d,
        Mut, Node, PositionType, Query, ResMut, Text, Transform, Val, With,
    },
    DefaultPlugins,
};
mod movable_circle;
use movable_circle::{Player, MOVE_SPEED};

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_systems(Startup, (setup_entity, setup_camera, setup_instructions))
        .add_systems(Update, move_player)
        .run();
}
fn setup_camera(mut commands: Commands) {
    commands.spawn(Camera2d);
}

fn setup_instructions(mut commands: Commands) {
    commands.spawn((
        Text(String::new()),
        Node {
            position_type: PositionType::Absolute,
            top: Val::Px(12.0),
            left: Val::Px(12.0),
            ..default()
        },
    ));
}
fn setup_entity(
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut commands: Commands,
) {
    commands.spawn((
        Player,
        Mesh2d(meshes.add(Circle { radius: 10.0 })),
        MeshMaterial2d(materials.add(Color::Srgba(Srgba::GREEN))),
    ));
}

fn move_player(
    mut transforms: Query<&mut Transform, With<Player>>,
    mut text: Query<&mut Text>,
    keys: Res<ButtonInput<KeyCode>>,
) {
    let mut text: Mut<'_, Text> = text.single_mut();

    transforms.iter_mut().for_each(|mut transform| {
        let direction: Vec3 = Vec3::new(
            (keys.pressed(KeyCode::KeyD)as i32 - keys.pressed(KeyCode::KeyA) as i32)as f32,
            (keys.pressed(KeyCode::KeyW) as i32 - keys.pressed(KeyCode::KeyS) as i32)as f32,
            0.0
        );
        if 0.0 < direction.length() {
            transform.translation += MOVE_SPEED * direction.normalize();
        }
        text.clear();
        text.push_str(
            format!(
                "Position\nx:{}\ny:{}\nDirection: x:{} y:{}",
                transform.translation.x, transform.translation.y, direction.x, direction.y
            )
            .as_str(),
        );
    });
}
