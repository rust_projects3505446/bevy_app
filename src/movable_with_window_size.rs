use bevy::{
    app::{App, Startup, Update},
    color::{Color, Srgba},
    ecs::change_detection::Res,
    input::{keyboard::KeyCode, ButtonInput},
    math::Vec3,
    prelude::{
        default, Assets, Camera2d, Circle, ColorMaterial, Commands, Component, Mesh, Mesh2d,
        MeshMaterial2d, Mut, Node, PositionType, Query, ResMut, Text, Transform, Val, Window, With,
    },
    DefaultPlugins,
};

use rand::{rngs::ThreadRng, Rng};
#[derive(Component)]
pub struct Player;
#[derive(Component)]
pub struct Entity;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_systems(Startup, (setup_entity, setup_camera, setup_instructions))
        .add_systems(Update, (move_player, move_entity))
        .run();
}
fn setup_camera(mut commands: Commands) {
    commands.spawn(Camera2d);
}

fn setup_instructions(mut commands: Commands) {
    commands.spawn((
        Text(String::new()),
        Node {
            position_type: PositionType::Absolute,
            top: Val::Px(12.0),
            left: Val::Px(12.0),
            ..default()
        },
    ));
}
fn setup_entity(
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut commands: Commands,
) {
    commands.spawn((
        Player,
        Mesh2d(meshes.add(Circle { radius: 10.0 })),
        MeshMaterial2d(materials.add(Color::Srgba(Srgba::GREEN))),
    ));
    (0..100).for_each(|_| {
        commands.spawn((
            Entity,
            Mesh2d(meshes.add(Circle { radius: 10.0 })),
            MeshMaterial2d(materials.add(Color::Srgba(Srgba::RED))),
        ));
    });
}
const MOVE_SPEED: f32 = 6.0;
fn move_player(
    mut transforms: Query<&mut Transform, With<Player>>,
    mut text: Query<&mut Text>,
    keys: Res<ButtonInput<KeyCode>>,
) {
    let mut text: Mut<'_, Text> = text.single_mut();

    transforms.iter_mut().for_each(|mut transform| {
        let direction: Vec3 = Vec3::new(
            (keys.pressed(KeyCode::KeyD)as i32 - keys.pressed(KeyCode::KeyA) as i32)as f32,
            (keys.pressed(KeyCode::KeyW) as i32 - keys.pressed(KeyCode::KeyS) as i32)as f32,
            0.0
        );
        
        if 0.0 < direction.length() {
            transform.translation += MOVE_SPEED * direction.normalize();
        }
        text.0=format!(
                "Position\nx:{}\ny:{}\nDirection: x:{} y:{}",
                transform.translation.x, transform.translation.y, direction.x, direction.y
            );
    });
}
fn calculate_translation(border: f32, current_number: f32, mut rng: ThreadRng) -> f32 {
    if current_number >= border / 2.0 {
        current_number - rng.random_range(0.0..40.0)
    } else if current_number <= -border / 2.0 {
        current_number + rng.random_range(0.0..40.0)
    } else {
        current_number + rng.random_range(-40.0..40.0)
    }
}
fn move_entity(mut transforms: Query<&mut Transform, With<Entity>>, windows: Query<&mut Window>) {
    let rng = rand::rng();
    transforms.iter_mut().for_each(|mut transform| {
        transform.translation.x = calculate_translation(
            windows.single().resolution.width(),
            transform.translation.x,
            rng.clone(),
        );
        transform.translation.y = calculate_translation(
            windows.single().resolution.height(),
            transform.translation.y,
            rng.clone(),
        );
    });
}
