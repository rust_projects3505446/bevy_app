use bevy::{
    prelude::*,
    color::palettes::css,
    core_pipeline::core_3d::Camera3d,
};
use rand::Rng;
use bevy_rapier3d::prelude::*;
#[derive(Component)]
struct Player;

#[derive(Component)]
struct Entity;

#[derive(Component,Clone)]
struct RotationText;

#[derive(Component)]
struct BodyTransform<T, G> {
    body: T,
    transform: G,
    color: Color,
}
fn main() {
    App::new().add_plugins((
        DefaultPlugins,
        RapierPhysicsPlugin::<NoUserData>::default(),
        RapierDebugRenderPlugin::default(),
        ))
        .add_systems(Startup, (setup_graphics, setup_physics, setup_textfields))
        .add_systems(
            Update,
            (show_ball_altitute, move_player,move_entitys),
        )
        .run();
}
fn setup_textfields(mut commands: Commands, windows: Query<&mut Window>) {
    let default_font_size = 30000.0 / windows.single().resolution.width();
    commands.spawn((
        Text::new("Rotation"),
        TextFont{
            font_size: default_font_size,
            ..default()
        },
        TextColor(Color::srgb(0.5, 0.5, 1.0)),
        Node{
            position_type: PositionType::Absolute,
            top: Val::Px(12.0),
            left: Val::Px(12.0),
            ..default()
        }
        )).with_child((
            TextSpan::default(),
            TextColor(css::GOLD.into()),
            TextFont{
                font_size: default_font_size,
                ..default()
            },
            RotationText
        ));
}
fn setup_graphics(mut commands: Commands) {
    // Add a camera so we can see the debug-render.
    commands.spawn((
        Camera3d::default(),
        Transform::from_xyz(-3.0, 3.0, 40.0)
        .looking_at(Vec3::ZERO, Vec3::Y),
    ));
}

fn setup_physics(
    mut commands: Commands,
    mut materials: ResMut<Assets<StandardMaterial>>,
    mut meshes: ResMut<Assets<Mesh>>,
) {
    /* Create the ground. */
    [BodyTransform {
        body: Vec3::new(100.0, 50.0, 100.0),
        transform: Transform::from_xyz(0.0, -54.0, 0.0),
        color: Color::srgb_u8(0, 255, 0),
    }]
    .iter()
    .for_each(|part| {
        commands
            .spawn((
                Collider::cuboid(part.body.x, part.body.y, part.body.z),
                Mesh3d(meshes.add(Cuboid::new(
                    part.body.x * 2.0,
                    part.body.y * 2.0,
                    part.body.z * 2.0,
                ))),
                MeshMaterial3d(materials.add(part.color)),
                part.transform
            ));
    });

    /* Create the bouncing ball. */
    [BodyTransform {
        body: 0.5,
        transform: Transform::from_xyz(0.0, 4.0, 0.0),
        color: Color::srgb_u8(50, 204, 255),
    }]
    .iter()
    .for_each(|part| {
        commands
            .spawn((
                RigidBody::Dynamic,
                Mesh3d(meshes.add(Sphere::new(part.body).mesh().ico(7).unwrap())),
                MeshMaterial3d(materials.add(part.color)),
                Collider::ball(part.body),
                Restitution::coefficient(0.7),
                part.transform,
                GravityScale(2.0),
                Player
            ));
    });
    (0..1000).map(|_|BodyTransform{
        body: 0.5,
        transform: Transform::from_xyz(rand::rng().random(), rand::rng().random(), rand::rng().random()),
        color: Color::srgb_u8(
            rand::rng().random_range(0..=255), 
            rand::rng().random_range(0..=255), 
            rand::rng().random_range(0..=255)),
    }).for_each(|part|{
        commands
           .spawn((
                RigidBody::Dynamic,
                Mesh3d(meshes.add(Sphere::new(part.body).mesh().ico(7).unwrap())),
                MeshMaterial3d(materials.add(part.color)),
                Collider::ball(part.body),
                Restitution::coefficient(0.7),
                part.transform,
                GravityScale(2.0),
                Entity
            ));
    });
}
fn show_ball_altitute(
    positions: Query<&mut Transform, With<RigidBody>>,
    mut text: Query<&mut TextSpan,With<RotationText>>,
) {
    positions.iter().for_each(|transform| {
        text.single_mut().0 = format!(
            "x:{}\n y:{}\n z:{}\n",
            transform.translation.x, transform.translation.y, transform.translation.z
        );
    });
}
fn move_entitys(
    mut transforms: Query<&mut Transform, With<Entity>>
){
    transforms.iter_mut().for_each(|mut transform| {
        transform.translation.y += rand::rng().random_range(-1.0..=1.0);
        transform.translation.x += rand::rng().random_range(-1.0..=1.0);
        transform.translation.z += rand::rng().random_range(-1.0..=1.0);
    });
}
const MOVE_SPEED: f32 = 0.5;
fn move_player(
    mut transforms: Query<&mut Transform, With<Player>>,
    keys: Res<ButtonInput<KeyCode>>,
) {
    transforms.iter_mut().for_each(|mut transform| {
        let mut direction = Vec3::ZERO;
        if keys.pressed(KeyCode::KeyW) {
            direction.z += 1.0;
        }
        if keys.pressed(KeyCode::KeyS) {
            direction.z -= 1.0;
        }
        if keys.pressed(KeyCode::KeyA) {
            direction.x -= 1.0;
        }
        if keys.pressed(KeyCode::KeyD) {
            direction.x += 1.0;
        }
        if keys.pressed(KeyCode::Space) {
            direction.y += 0.25;
        }
        if 0.0 < direction.length() {
            transform.translation += MOVE_SPEED * direction.normalize();
        }
    });
}
