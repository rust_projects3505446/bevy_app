use bevy::prelude::*;
use bevy_rapier3d::prelude::*;
use rapier3d_urdf::{UrdfLoaderOptions, UrdfMultibodyOptions, UrdfRobot};
use std::path::Path;

pub struct UrdfLoaderPlugin;

impl Plugin for UrdfLoaderPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, load_urdf);
    }
}

fn load_urdf(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    asset_server: Res<AssetServer>,
) {
}
