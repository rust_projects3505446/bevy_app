use bevy::{
    color::palettes::css,
    prelude::*,
};
use bevy_rapier2d::prelude::*;
#[derive(Component)]
struct Cuboid {
    body: Vec2,
    transform: Vec3,
}
#[derive(Component)]
struct PositionText;

#[derive(Component, Clone)]
struct Player{
    movement_speed: f32,
    rotation_speed: f32,
}

fn main() {
    App::new()
        .add_plugins((
            DefaultPlugins,
            RapierPhysicsPlugin::<NoUserData>::pixels_per_meter(300.0),
            RapierDebugRenderPlugin::default(),
        ))
        .add_systems(Startup, (setup_graphics, setup_physics, setup_textfields))
        .add_systems(Update, (print_ball_altitude, move_player,force_player))
        .run();
}
fn setup_graphics(mut commands: Commands) {
    // Add a camera so we can see the debug-render.
    commands.spawn(Camera2d);
}
fn setup_textfields(mut commands: Commands, windows: Query<&mut Window>) {
    let default_font_size = 30000.0 / windows.single().resolution.width();
    commands.spawn((
        Text::new("Ball altitude:"),
        Node {
            position_type: PositionType::Absolute,
            top: Val::Px(12.0),
            left: Val::Px(12.0),
            ..default()
        },
        TextFont{
            font_size: default_font_size,
            ..default()
        },
        TextColor(Color::srgb(1.0, 0.5, 0.5)),
    )).with_child((
        TextSpan::default(),
        TextColor(css::GOLD.into()),
        TextFont{
            font_size: default_font_size,
            ..default()
        },
        PositionText
    ));
}
fn setup_physics(
    mut commands: Commands,
    windows: Query<&mut Window>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut meshes: ResMut<Assets<Mesh>>,
) {
    [
        Cuboid {
            body: Vec2::new(
                windows.single().width() / 3.0,
                windows.single().height() / 30.0,
            ),
            transform: Vec3::new(0.0, -100.0, 0.0),
        },
        Cuboid {
            body: Vec2::new(windows.single().width() / 2.0, 25.0),
            transform: Vec3::new(0.0, -windows.single().height() / 2.0, 0.0),
        },
        Cuboid {
            body: Vec2::new(10.0, windows.single().height() / 2.0),
            transform: Vec3::new(windows.single().width() / 2.0, 0.0, 0.0),
        },
        Cuboid {
            body: Vec2::new(10.0, windows.single().height() / 2.0),
            transform: Vec3::new(-windows.single().width() / 2.0, 0.0, 0.0),
        },
    ]
    .iter()
    .for_each(|cuboid| {
        commands
            .spawn((
                Collider::cuboid(cuboid.body.x, cuboid.body.y),
                Mesh2d(meshes.add(Rectangle::from_size(cuboid.body * 2.0))),
                MeshMaterial2d(materials.add(Color::Srgba(css::ORANGE))),
                Transform::from(Transform::from_translation(cuboid.transform,)),
                )   
            );
    });
    /* Create the bouncing ball. */
    let ball_size: f32 = 50.0;
    commands
        .spawn((
            RigidBody::Dynamic,
            Mesh2d(meshes.add(Circle { radius: ball_size })),
            MeshMaterial2d(materials.add(Color::Srgba(Srgba::GREEN))),
            Collider::ball(ball_size),
            Restitution::coefficient(1.13),
            Transform::from_xyz(0.0, 400.0, 0.0),
            ExternalForce::default(),
            Damping {
                linear_damping: 0.,
                angular_damping: 1.0,
            },
            Player{
                movement_speed: 6.0,
                rotation_speed: 500000.0,
            },
        ));
}

fn print_ball_altitude(
    positions: Query<&Transform, With<RigidBody>>,
    mut text: Query<&mut TextSpan>,
) {
    positions.iter().for_each(|transform| {
        text.single_mut().0=format!("{}", transform.translation.y);
    });
}

fn move_player(
    mut transforms: Query<(&Player, &mut Transform), With<RigidBody>>,
    keys: Res<ButtonInput<KeyCode>>,
) {
    transforms.iter_mut().for_each(|(player,mut transform)| {
        let direction: Vec3 = Vec3::new(
            (keys.pressed(KeyCode::KeyD)as i32 - keys.pressed(KeyCode::KeyA) as i32)as f32,
            (keys.pressed(KeyCode::KeyW) as i32 - keys.pressed(KeyCode::KeyS) as i32)as f32,
            0.0
        );
        if 0.0 < direction.length() {
            transform.translation += player.movement_speed * direction.normalize();
        }
    });
}
fn force_player(
    mut ball_query: Query<(&Player, &mut ExternalForce), With<RigidBody>>,
    keys: Res<ButtonInput<KeyCode>>,
) {    
    ball_query.iter_mut().for_each(
        |(player, mut external_force)|{
        external_force.force.x =(keys.pressed(KeyCode::KeyE) as i32 -keys.pressed(KeyCode::KeyQ)as i32) as f32 * player.rotation_speed;
    }); 
}

