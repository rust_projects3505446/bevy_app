use bevy::{
    color::palettes::basic::{BLUE, YELLOW},
    prelude::*,
};
use bevy_rapier2d::prelude::*;
fn main() {
    App::new()
        .add_plugins((
            DefaultPlugins,
            RapierPhysicsPlugin::<NoUserData>::default()
        ))
        .add_systems(Startup, (setup_camera, setup))
        .run();
}
fn setup_camera(mut commands: Commands) {
    commands.spawn(Camera2d);
}
#[derive(Component, Debug, Clone, Copy)]
struct Body<T, G> {
    body: T,
    transform: G,
    color: Color,
    rigid: RigidBody,
    vel: Velocity,
}
fn setup(
    mut commands: Commands,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut meshes: ResMut<Assets<Mesh>>,
) {
    // Sun
    let entitys: Vec<Entity> = [
        //sun
        Body {
            body: 50.0_f32,
            transform: Vec3::new(0.0, 0.0, 0.0),
            color: Color::Srgba(YELLOW),
            rigid: RigidBody::Fixed,
            vel: Velocity {
                linvel: Vec2::new(0.0, 0.0),
                angvel: 0.0,
            },
        },
        //Planet
        Body {
            body: 10.0_f32,
            transform: Vec3::new(100.0, 0.0, 0.0),
            color: Color::Srgba(BLUE),
            rigid: RigidBody::Dynamic,
            vel: Velocity {
                linvel: Vec2::new(0.0, 100.0),
                angvel: 0.4,
            },
        },
    ]
    .into_iter()
    .map(|body| {
        commands
            .spawn((
                body.rigid,
                Collider::ball(body.body),
                Mesh2d(meshes.add(Circle { radius: body.body })),
                MeshMaterial2d(materials.add(body.color)),
                Transform::from_translation(body.transform),
                body.vel
            ))
            .id()
    })
    .collect::<Vec<Entity>>();
    commands.entity(entitys[1]).with_children(|cmd| {
        cmd.spawn(ImpulseJoint::new(
            entitys[0],
            RevoluteJointBuilder::new().local_anchor2(Vec2::new(100.0, 0.0)),
        ));
    });
}
