use bevy::{color::palettes::css, prelude::*};
use bevy_rapier2d::prelude::*;
#[derive(Component)]
struct Body<T, G> {
    body: T,
    transform: G,
}

fn main() {
    App::new()
        .add_plugins((
            DefaultPlugins,
            RapierPhysicsPlugin::<NoUserData>::pixels_per_meter(300.0),
            RapierDebugRenderPlugin::default(),
        ))
        .add_systems(Startup, (setup_graphics, setup_physics, setup_textfields))
        .add_systems(Update, (print_ball_altitude, move_player))
        .run();
}
fn setup_graphics(mut commands: Commands) {
    // Add a camera so we can see the debug-render.
    commands.spawn(Camera2d);
}
fn setup_textfields(mut commands: Commands, windows: Query<&mut Window>) {
    let default_font_size = 30000.0 / windows.single().resolution.width();
    commands.spawn((
        Text(String::from("Ball altitude:")),
        Node {
            position_type: PositionType::Absolute,
            top: Val::Px(12.0),
            left: Val::Px(12.0),
            ..default()
        },
        TextFont {
            font_size: default_font_size,
            ..default()
        },
        TextColor(Color::srgb(1.0, 0.5, 0.5)),
    ));
}
fn setup_physics(
    mut commands: Commands,
    windows: Query<&mut Window>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut meshes: ResMut<Assets<Mesh>>,
) {
    [
        Body {
            body: Vec2::new(
                windows.single().width() / 3.0,
                windows.single().height() / 30.0,
            ),
            transform: Vec3::new(0.0, -100.0, 0.0),
        },
        Body {
            body: Vec2::new(windows.single().width() / 2.0, 25.0),
            transform: Vec3::new(0.0, -windows.single().height() / 2.0, 0.0),
        },
        Body {
            body: Vec2::new(10.0, windows.single().height() / 2.0),
            transform: Vec3::new(windows.single().width() / 2.0, 0.0, 0.0),
        },
        Body {
            body: Vec2::new(10.0, windows.single().height() / 2.0),
            transform: Vec3::new(-windows.single().width() / 2.0, 0.0, 0.0),
        },
    ]
    .iter()
    .for_each(|cuboid| {
        commands
            .spawn(Collider::cuboid(cuboid.body.x, cuboid.body.y))
            .insert((
                Mesh2d(meshes.add(Rectangle::from_size(cuboid.body * 2.0))),
                MeshMaterial2d(materials.add(Color::Srgba(css::ORANGE))),
                Transform::from_translation(cuboid.transform),
            ));
    });
    /* Create the bouncing ball. */
    let entitys: Vec<Entity> = [
        Body {
            body: 50.0_f32,
            transform: Vec3::new(0.0, 400.0, 0.0),
        },
        Body {
            body: 50.0_f32,
            transform: Vec3::new(0.0, 400.0, 0.0),
        },
    ]
    .iter()
    .map(|body| {
        commands
            .spawn((
                RigidBody::Dynamic,
                Mesh2d(meshes.add(Circle { radius: body.body })),
                MeshMaterial2d(materials.add(Color::Srgba(Srgba::GREEN))),
                Collider::ball(body.body),
                Restitution::coefficient(1.13),
                Transform::from_translation(body.transform),
                GravityScale(1.0),
            ))
            .id()
    })
    .collect::<Vec<Entity>>();
    commands.entity(entitys[1]).with_children(|cmd| {
        cmd.spawn(ImpulseJoint::new(
            entitys[0],
            RevoluteJointBuilder::new().local_anchor2(Vec2::new(0.0, 200.0)),
        ));
    });
}

fn print_ball_altitude(
    positions: Query<&Transform, With<RigidBody>>,
    mut position: Query<&mut Text>,
) {
    positions.iter().for_each(|transform| {
        position.single_mut().0 = format!("{}", transform.translation.y);
    });
}
const MOVE_SPEED: f32 = 6.0;

fn move_player(
    mut transforms: Query<&mut Transform, With<RigidBody>>,
    keys: Res<ButtonInput<KeyCode>>,
) {
    transforms.iter_mut().for_each(|mut transform| {
        let direction: Vec3 = Vec3::new(
            (keys.pressed(KeyCode::KeyD)as i32 - keys.pressed(KeyCode::KeyA) as i32)as f32,
            (keys.pressed(KeyCode::KeyW) as i32 - keys.pressed(KeyCode::KeyS) as i32)as f32,
            0.0
        );
        if 0.0 < direction.length() {
            transform.translation += MOVE_SPEED * direction.normalize();
        }
    });
}
