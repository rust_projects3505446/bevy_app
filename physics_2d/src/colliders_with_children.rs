use bevy::prelude::*;
use bevy_rapier2d::prelude::*;
fn main() {
    App::new()
        .add_plugins((
            DefaultPlugins,
            RapierPhysicsPlugin::<NoUserData>::pixels_per_meter(300.0),
            RapierDebugRenderPlugin::default(),
        ))
        .add_systems(Startup, (setup_graphics, setup_physics))
        .add_systems(Update, (print_ball_altitude, move_player))
        .run();
}
fn setup_graphics(mut commands: Commands) {
    commands.spawn(Camera2d);
}
fn setup_physics(mut commands: Commands) {
    // Attach a single collider to a rigid-body.
    commands.spawn((RigidBody::Dynamic, Collider::ball(0.5)));

    // Attach a multiple colliders to a rigid-body.
    commands
        .spawn((RigidBody::Dynamic, GlobalTransform::default()))
        .with_children(|children| {
            children.spawn((Collider::ball(0.5), Transform::from_xyz(0.0, 0.0, -1.0)));
            // Position the collider relative to the rigid-body.
            children.spawn((Collider::ball(0.5), Transform::from_xyz(0.0, 0.0, 1.0)));
            // Position the collider relative to the rigid-body.
        });
}

fn print_ball_altitude(
    positions: Query<&Transform, With<RigidBody>>,
    mut position: Query<&mut Text>,
) {
    positions.iter().for_each(|transform| {
        position.single_mut().0 = format!("{}", transform.translation.y);
    });
}
const MOVE_SPEED: f32 = 6.0;

fn move_player(
    mut transforms: Query<&mut Transform, With<RigidBody>>,
    keys: Res<ButtonInput<KeyCode>>,
) {
    transforms.iter_mut().for_each(|mut transform| {
        let direction: Vec3 = Vec3::new(
            (keys.pressed(KeyCode::KeyD) as i32 - keys.pressed(KeyCode::KeyA) as i32) as f32,
            (keys.pressed(KeyCode::KeyW) as i32 - keys.pressed(KeyCode::KeyS) as i32) as f32,
            0.0,
        );
        if 0.0 < direction.length() {
            transform.translation += MOVE_SPEED * direction.normalize();
        }
    });
}
