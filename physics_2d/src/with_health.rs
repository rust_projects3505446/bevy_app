use bevy::{
    color::palettes::css,
    prelude::*,
    math::prelude::Capsule2d,
};
use rand::Rng;
use bevy_rapier2d::prelude::*;
#[derive(Component)]
struct Cuboid {
    body: Vec2,
    transform: Vec3,
}
#[derive(Component)]
struct PositionText;

#[derive(Component,Clone)]
struct Player{
    movement_speed: f32,
    rotation_speed: f32,
}

#[derive(Component)]
struct HealthInfo;
#[derive(Component)]
struct Health(i8);

#[derive(Component)]
struct NonPlayerBall; // New component to mark non-player balls

const initial_health:i8 = 3;

fn main() {
    App::new()
        .add_plugins((
            DefaultPlugins,
            RemotePlugin::default(),
            RemoteHttpPlugin::default(),
            RapierPhysicsPlugin::<NoUserData>::pixels_per_meter(300.0),
            RapierDebugRenderPlugin::default(),
        ))
        .add_systems(Startup, (setup_graphics, setup_physics, setup_textfields))
        .add_systems(Update, (
            print_ball_altitude, move_player,render_health_info,force_player,handle_collisions
        ))
        .run();
}
fn setup_graphics(mut commands: Commands) {
    // Add a camera so we can see the debug-render.
    commands.spawn(Camera2d);
}
fn setup_textfields(mut commands: Commands, windows: Query<&mut Window>) {
    let default_font_size = 30000.0 / windows.single().resolution.width();
    commands.spawn((
        Text::new("Ball altitude:"),
        Node {
            position_type: PositionType::Absolute,
            top: Val::Px(12.0),
            left: Val::Px(12.0),
            ..default()
        },
        TextFont{
            font_size: default_font_size,
            ..default()
        },
        TextColor(Color::srgb(1.0, 0.5, 0.5)),
    )).with_child((
        TextSpan::default(),
        TextColor(css::GOLD.into()),
        TextFont{
            font_size: default_font_size,
            ..default()
        },
        PositionText
    ));
    commands.spawn((
        Text::new("Health: "),
        Node{
            position_type: PositionType::Absolute,
            top: Val::Px(34.0),
            left: Val::Px(12.0),
            ..default()
        }
    )).with_child((
        TextSpan::new(format!("{}", initial_health)),
        TextColor(css::GOLD.into()),
        TextFont{
            font_size: default_font_size,
            ..default()
        },
        HealthInfo
    ));
}
fn setup_physics(
    mut commands: Commands,
    windows: Query<&mut Window>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut meshes: ResMut<Assets<Mesh>>,
) {
    [
        Cuboid {
            body: Vec2::new(
                windows.single().width() / 3.0,
                windows.single().height() / 30.0,
            ),
            transform: Vec3::new(0.0, -100.0, 0.0),
        },
        Cuboid {
            body: Vec2::new(windows.single().width() / 2.0, 25.0),
            transform: Vec3::new(0.0, -windows.single().height() / 2.0, 0.0),
        },
        Cuboid {
            body: Vec2::new(10.0, windows.single().height() / 2.0),
            transform: Vec3::new(windows.single().width() / 2.0, 0.0, 0.0),
        },
        Cuboid {
            body: Vec2::new(10.0, windows.single().height() / 2.0),
            transform: Vec3::new(-windows.single().width() / 2.0, 0.0, 0.0),
        },
    ]
    .iter()
    .for_each(|cuboid| {
        commands
            .spawn((
                Collider::cuboid(cuboid.body.x, cuboid.body.y),
                Mesh2d(meshes.add(Rectangle::from_size(cuboid.body * 2.0))),
                MeshMaterial2d(materials.add(Color::Srgba(css::ORANGE))),
                Transform::from(Transform::from_translation(cuboid.transform,)),
                )   
            );
    });
    /* Create the bouncing ball. */
    let ball_size: f32 = 50.0;
    commands
        .spawn((
            ActiveEvents::COLLISION_EVENTS,
            RigidBody::Dynamic,
            Mesh2d(meshes.add( Capsule2d{ 
                radius: ball_size,
                half_length: ball_size, 
            })),
            ExternalForce::default(),
            MeshMaterial2d(materials.add(Color::Srgba(Srgba::GREEN))),
            Collider::capsule_y(ball_size, ball_size),
            Restitution::coefficient(1.13),
            Transform::from(Transform::from_xyz(0.0, 400.0, 0.0)),
            Health(initial_health),
            Damping {
                linear_damping: 0.,
                angular_damping: 3.0,
            },
            Player{
                movement_speed: 6.0,
                rotation_speed: 500000.0,
            }
        ));
        (0..5).for_each(|_|{
        commands.spawn((
            ActiveEvents::COLLISION_EVENTS,
            RigidBody::Dynamic,
            Mesh2d(meshes.add(Circle { radius: ball_size })),
            MeshMaterial2d(materials.add(Color::Srgba(
                Srgba { 
                    red: rand::rng().random_range(0.0..=1.0), 
                    green: rand::rng().random_range(0.0..=1.0), 
                    blue: rand::rng().random_range(0.0..=1.0), 
                    alpha: 1.0 
                }
            ))),
            Collider::ball(ball_size),
            Restitution::coefficient(1.0),
            Transform::from(Transform::from_xyz(
                rand::rng().random(), 
                rand::rng().random(), 0.0)),
            NonPlayerBall,
        ));}
    );
    
}

fn print_ball_altitude(
    positions: Query<&Transform, With<RigidBody>>,
    mut text: Query<&mut TextSpan,With<PositionText>>,
) {
    positions.iter().for_each(|transform| {
        text.single_mut().0=format!("{}", transform.translation.y);
    });
}
fn render_health_info(
    player_query: Query<&mut Health, With<RigidBody>>,
    mut health_info_query: Query<&mut TextSpan, With<HealthInfo>>,
) {
    health_info_query.single_mut().0 = format!("{}", player_query.single().0);
}
fn move_player(
    mut transforms: Query<(&Player,&mut Transform), With<Health>>,
    keys: Res<ButtonInput<KeyCode>>,
) {   
    transforms.iter_mut().for_each(|(player, mut transform)| {
        let direction: Vec3 = Vec3::new(
            (keys.pressed(KeyCode::KeyD)as i32 - keys.pressed(KeyCode::KeyA) as i32)as f32,
            (keys.pressed(KeyCode::KeyW) as i32 - keys.pressed(KeyCode::KeyS) as i32)as f32,
            0.0
        );
        if 0.0 < direction.length() {
            transform.translation += player.movement_speed * direction.normalize();
        }
    });
}

fn force_player(
    mut ball_query: Query<(&Player, &mut ExternalForce), With<RigidBody>>,
    keys: Res<ButtonInput<KeyCode>>,
) {    
    ball_query.iter_mut().for_each(
        |(player, mut external_force)|{
        external_force.force.x =(
            keys.pressed(KeyCode::KeyE) as i32 -keys.pressed(KeyCode::KeyQ)as i32
        ) as f32 * player.rotation_speed;
    }); 
}

fn handle_collisions(
    mut collision_events: EventReader<CollisionEvent>,
    player_query: Query<Entity, With<Player>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    non_player_materials: Query<&MeshMaterial2d<ColorMaterial>, With<NonPlayerBall>>,
) {
    let Ok(player_entity) = player_query.get_single() else { return };

    for event in collision_events.read() {
        if let CollisionEvent::Started(entity_a, entity_b, _) = event {
            let other_entity = if *entity_a == player_entity {
                entity_b
            } else if *entity_b == player_entity {
                entity_a
            } else {
                continue;
            };

            if let Ok(mesh_material) = non_player_materials.get(*other_entity) { 
                if let Some(material) = materials.get_mut(&mesh_material.0) {
                    material.color = Color::srgba(
                        rand::rng().random_range(0.0..=1.0),
                        rand::rng().random_range(0.0..=1.0),
                        rand::rng().random_range(0.0..=1.0),
                        1.0,
                    );
                }
            }
        }
    }
}