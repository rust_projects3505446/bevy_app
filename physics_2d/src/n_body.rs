use bevy::{
    color::palettes::basic::{BLUE, YELLOW},
    prelude::{Color::Srgba, *},
    core_pipeline::core_2d::Camera2d,
};
use bevy_rapier2d::prelude::*;
const GRAVITY_CONSTANT: f32 = 758.0;
const SUN_MASS: f32 = GRAVITY_CONSTANT;
const PLANET_MASS: f32 = 1.0;
const INITIAL_DISTANCE: f32 = 200.0;
const INITIAL_VELOCITY: f32 = 7.0;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugins(RapierPhysicsPlugin::<NoUserData>::pixels_per_meter(100.0))
        .add_systems(Startup, (setup, setup_planets))
        .run();
}

#[derive(Component)]
struct Sun;

#[derive(Component, Clone)]
struct Planet {
    radius: f32,
    color: Color,
    transform: Vec3,
}

fn setup(mut commands: Commands) {
    // Camera
    commands.spawn(Camera2d);
}
fn setup_planets(
    mut commands: Commands,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut meshes: ResMut<Assets<Mesh>>,
) {
    // Sun
    [25.0].into_iter().for_each(|radius| {
        commands
            .spawn((
                Mesh2d(meshes.add(Circle { radius })),
                MeshMaterial2d(materials.add(Srgba(YELLOW))),
                RigidBody::Fixed,
                Sun,
                AdditionalMassProperties::Mass(SUN_MASS)
            ));
    });

    // Planet
    [Planet {
        radius: 10.0,
        color: Srgba(BLUE),
        transform: Vec3::new(INITIAL_DISTANCE, 0.0, 0.0),
    }]
    .into_iter()
    .for_each(|planet| {
        commands
            .spawn((
                Mesh2d(meshes.add(Circle {radius: planet.radius,})),
                MeshMaterial2d(materials.add(planet.color)),
                RigidBody::Dynamic,
                Velocity {
                    linvel: Vec2::new(0.0, INITIAL_VELOCITY),
                    angvel: 0.0,
                },
                Collider::ball(10.0),
                planet.clone(),
                Transform::from_xyz(
                    planet.transform.x,
                    planet.transform.y,
                    planet.transform.z,
                )
            ));
    });
}
